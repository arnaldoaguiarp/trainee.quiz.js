var questions;// Receber os dados var 
n = -1; // Auxiliar para indicar a pergunta
var points = 0; // Acumula a pontuação
var mostraResposta = document.getElementsByTagName("span");// Variável que irá auxiliar na troca das respostas
var respostaMarcada = document.getElementsByName("resposta");// Variável que irá identificar a resposta marcada


httpRequest = new XMLHttpRequest();// Criar uma instância de XMLHttpRequest

httpRequest.open('GET', 'https://quiz-trainee.herokuapp.com/questions', true);//Recebe os dados e indica de onde
httpRequest.send(); //Envia os dados para o servidor

httpRequest.onreadystatechange = function(){ 
    if(this.readyState == 4 && this.status == 200){ 
        questions = (JSON.parse(this.responseText));//analisa uma string JSON, construindo um objeto descrito pela string 
    }
};

function mostrarQuestao() { 
    
    console.log(questions);// Auxilia na trocas das telas do quiz. PS: não é do JavaScript 
    
    if (n == -1) { // Inicia o quiz 
        document.getElementById("listaRespostas").style.display = "inline" ; // Elemento fica "embutido"
        document.getElementById("resultado").style.display = 'none' ; // Não é mostrado
        
        document.getElementById("confirmar").innerHTML = "PRÓXIMA" ;
    } else {
        for (var i = 0; i < 4; i++){
            if (document.getElementsByName("resposta")[i].checked) // Identfica se foi selecionado alguma opção
                break; // Sai do laço e continua
            else if (i==3)// Chegou no final e não obteve sucesso, 
                return; // Volta para o inicio do laço
        }         
    }
    /*if(document.getElementsByName("input").checked==true){
            n++; // Chama a pergunta posterior
        }else{
            return
    }*/
    
    n++; // Chama a pergunta posterior
    

    /*for(var i=0; i<questions[n].options.length ; i++){
        if(document.getElementsByName("input")[i].checked==true){
            n++; // Chama a pergunta posterior
        }else{
            return
        }
    }*/
    
    if(n >= questions.length){ // Verifica se existem mais perguntas, caso não exista, ele finaliza o quiz 
        
        finalizarQuiz(); 
        return; 
        
    } 
    

    document.getElementById('titulo').innerHTML = questions[n].title; // Atualiza o titulo da pergunta
    
    //document.getElementById('resposta') = questions[n].value; // Atualiza os valores das respostas

    for (var k = 0; k < questions[n].options.length; k++){  // Vai atualizando de acordo com o número de questões do quiz

        if(respostaMarcada[k].checked){
            
            respostaMarcada[k].value = questions[n].options[k].value;
            points = points + parseInt(respostaMarcada[k].value);// Adiciona aos pontos o valor da resposta da pergunta escolhida 
            respostaMarcada[k].checked = false;// Deseleciona para a próxima pergunta
        }
        
        /*if(document.getElementsByName("input")[i].checked==true){
            break
        }else{
            return
        }*/

        mostraResposta[i].innerHTML = questions[n].options[i].answer; // Chama as próximas respostas
        
        /* Ajustar os valores das respostas quando atualiza a pergunta 
        --> questions[n].options[i].value*/
        
        //respostaMarcada[i].value = questions[n].value // Atualiza os valores das respostas

    }
}

function finalizarQuiz() { 
    
    var pontuacaoGeral = Math.round((points / 9) * 100); // Calculo da pontuação geral
    
    n = -1 ; // Seta a variável para reiniciar o quiz na próxima tela
    points = 0; // Após o calculo, a variável que guarda os pontos pode ser zerada
    
    document.getElementById('titulo').innerHTML = 'QUIZ DOS VALORES DA GTI' ;
    document.getElementById("listaRespostas").style.display = "none" ;
    document.getElementById("resultado").style.display = 'block' ;
    document.getElementById("resultado").innerHTML = 'Sua pontuação: ' + pontuacaoGeral + '%' ;
    document.getElementById("confirmar").innerHTML = "Refazer Quiz";
}